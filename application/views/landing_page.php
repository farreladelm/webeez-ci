<?php $this->load->view('_partial/navbar') ?>
<!-- HERO SECTION -->

<section class="hero shadow" id="hero-sec">
    <div class="hero-text">
        <h1 class="main-text">Tingkatkan profit bisnis anda dengan Webeez,
        </h1>
        <p>Perusahaan jasa pembuatan website</p>
        <div class="btn-cta">
            Lets Work!
        </div>
    </div>
    <div class="hero-img">
        <img src="<?= base_url("assets") ?>/asset/hero.png" alt="hero">
    </div>
</section>

<!-- END HERO SECTION -->

<!-- CLIENT SECTION -->

<section class="sec-listener client-sec shadow" id="client-sec">
    <div class="client-content">
        <div class="main-text">
            Klien Kami
        </div>
        <div class="clients">
            <div class="client ex-neu">
                <img id="client-1" src="<?= base_url("assets") ?>/asset/client/1.png" alt="client-1">
            </div>
            <div class="client ex-neu">
                <img id="client-2" src="<?= base_url("assets") ?>/asset/client/2.png" alt="client-2">
            </div>
            <div class="client ex-neu">
                <img id="client-3" src="<?= base_url("assets") ?>/asset/client/3.png" alt="client-3">
            </div>
        </div>
    </div>
    <div class="client-desc">
        <p>
            Webeez telah bekerjasama dengan perusahaan-perusahaan besar di Indonesia selama <strong>5 tahun</strong> di berbagai sektor industri.
        </p>
    </div>
</section>

<!-- END CLIENT SECTION -->

<!-- LAYANAN SECTION -->

<section class="sec-listener layanan-sec shadow" id="layanan-sec">
    <div class="main-text layanan-text">
        Kenapa Webeez?
    </div>
    <div class="layanan-content">
        <div class="layanan-img">
            <img src="<?= base_url("assets") ?>/asset/layanan-sec.png" alt="img">
        </div>
        <div class="text">
            <p>
                <strong>Webeez</strong> terbukti memberikan pengaruh yang baik kepada perusahaan-perusahaan di Indonesia dengan meningkatkan profit serta kenyamanan pengguna dari setiap situs web yang kami buat. <br><br>
                <strong>Webeez</strong> menyediakan jasa pembuatan situs web terbaik kepada klien dengan tingkat profesionalitas yang tinggi.
            </p>
        </div>
    </div>
    <div class="layanan-cards">

        <!-- CARD 1 -->

        <div class="card ex-neu">
            <div class="card-icon">
                <div class="title">Teknologi Terkini</div>
                <div class="icon in-neu">
                    <i class="fa-solid fa-microchip"></i>
                </div>
            </div>
            <div class="card-desc active">
                <div class="title">Teknologi Terkini</div>
                <div class="desc in-neu">Mengembangkan kebutuhan website maupun mobile Anda menggunakan teknologi up-to-date</div>
            </div>
        </div>

        <!-- CARD 2 -->

        <div class="card ex-neu">
            <div class="card-icon">
                <div class="title">Pemeliharaan Berkelanjutan</div>
                <div class="icon in-neu">
                    <i class="fa-solid fa-rotate"></i>
                </div>
            </div>
            <div class="card-desc active">
                <div class="title">Pemeliharaan Berkelanjutan</div>
                <div class="desc in-neu">Menyediakan layanan pemeliharaan yang berkelanjutan selama lifetime</div>
            </div>
        </div>

        <!-- CARD 3 -->

        <div class="card ex-neu">
            <div class="card-icon">
                <div class="title">Profesional</div>
                <div class="icon in-neu">
                    <i class="fa-solid fa-user-tie"></i>
                </div>
            </div>
            <div class="card-desc active">
                <div class="title">Profesional</div>
                <div class="desc in-neu">Project akan dikerjakan oleh tim kami yang profesional di bidangnya</div>
            </div>
        </div>

        <!-- CARD 4 -->

        <div class="card ex-neu">
            <div class="card-icon">
                <div class="title">Berpengalaman</div>
                <div class="icon in-neu">
                    <i class="fa-solid fa-ranking-star"></i>
                </div>
            </div>
            <div class="card-desc active">
                <div class="title">Berpengalaman</div>
                <div class="desc in-neu">Rekam jejak yang sudah terbukti dari berbagai testimoni</div>
            </div>
        </div>

        <!-- CARD 5 -->

        <div class="card ex-neu">
            <div class="card-icon">
                <div class="title">Pengerjaan Cepat</div>
                <div class="icon in-neu">
                    <i class="fa-regular fa-hourglass-half"></i>
                </div>
            </div>
            <div class="card-desc active">
                <div class="title">Pengerjaan Cepat</div>
                <div class="desc in-neu">Rekam jejak yang sudah terbukti dari berbagai testimoni</div>
            </div>
        </div>

        <!-- CARD 6 -->

        <div class="card ex-neu">
            <div class="card-icon">
                <div class="title">Keamanan Terjamin</div>
                <div class="icon in-neu">
                    <i class="fa-solid fa-user-shield"></i>
                </div>
            </div>
            <div class="card-desc active">
                <div class="title">Keamanan Terjamin</div>
                <div class="desc in-neu">Kerahasiaan data dan asset Anda akan terjaga</div>
            </div>
        </div>
    </div>
    <p>*Tekan untuk menampilkan detail</p>

    <!-- END LAYANAN CARD -->

    <!-- CUSTOM SHAPE -->

    <div class="circle in-neu">
        <div class="ex-neu"></div>
    </div>
    <div class="elipse"></div>
</section>

<!-- PROVIDE SECTION -->

<section class="sec-listener provide-sec">
    <div class="provide-text">
        <h1 class="main-text">Apa yang Kami Tawarkan?</h1>
        <p>Webeez akan memberikan berbagai macam layanan terbaik dalam pembuatan website bisnis anda.</p>
        <div class="c-text">
            <div></div>
        </div>
    </div>
    <div class="provide-items">
        <div class="provide-item-con">
            <div class="provide-item in-neu">
                <div class="item-heading">
                    <i class="fa-solid fa-computer"></i>
                    <span>Custom Application Development</span>
                </div>
                <p>Mengembangkan aplikasi custom berbasis web dan mobile dengan tampilan interaktif</p>
            </div>
            <div class="provide-item in-neu">
                <div class="item-heading">
                    <i class="fa-solid fa-sitemap"></i>
                    <span>Hardware and IT Infrastructure</span>
                </div>
                <p>Mengadakan komputer server, laptop, PC (Personal Computer), dan kebutuhan infrastruktur untuk memenuhi kebutuhan IT</p>
            </div>
            <div class="provide-item in-neu">
                <div class="item-heading">
                    <i class="fa-solid fa-chart-simple"></i>
                    <span>Digital Marketing Service</span>
                </div>
                <p>Capai target market Anda menggunakan Google Bisnis, Marketplace, dan strategi pemasaran digital lainnya</p>
            </div>
        </div>
        <div class="c-items">
            <div></div>
        </div>
    </div>
</section>

<!-- END PROVIDE SECTION -->

<!-- PORTFOLIO SECTION -->

<section class="sec-listener porto-sec shadow" id="porto-sec">
    <div class="porto-heading">
        <h1 class="main-text">
            Portofolio
        </h1>
    </div>
    <div class="porto-content">
        <div class="porto-text">
            Berikut merupakan beberapa hasil kerja sama <strong>Webeez</strong> dengan beberapa perusahaan besar di Indonesia.
            <br><br>
            Kami memiliki kapabilitas untuk membuat berbagai jenis situs web, seperti e-commerce, profile perusahaan, hingga situs web khusus suatu perusahaan.
        </div>
        <div class="porto-carousel">
            <img src="<?= base_url("assets") ?>/asset/portofolio/1.png" alt="1">
            <img src="<?= base_url("assets") ?>/asset/portofolio/2.png" alt="2">
            <img src="<?= base_url("assets") ?>/asset/portofolio/3.png" alt="3">
            <img src="<?= base_url("assets") ?>/asset/portofolio/4.png" alt="4">
            <img src="<?= base_url("assets") ?>/asset/portofolio/5.png" alt="5">
            <img src="<?= base_url("assets") ?>/asset/portofolio/6.png" alt="6">
            <img src="<?= base_url("assets") ?>/asset/portofolio/7.png" alt="7">
            <img src="<?= base_url("assets") ?>/asset/portofolio/8.png" alt="8">
            <img src="<?= base_url("assets") ?>/asset/portofolio/9.png" alt="9">
            <div></div>
            <span></span>
        </div>
    </div>
</section>

<!-- END PORTOFOLIO SECTION -->

<!-- CONTACT SECTION -->

<section class="sec-listener contact-sec" id="contact-sec">
    <div class="contact-img">
        <img src="<?= base_url("assets") ?>/asset/contact.png" alt="contact-img">
    </div>
    <div class="contact-content">
        <p>
            Ayo kerjasama dengan <strong>Webeez</strong> dan rasakan kemudahan membentuk dan mengelola website bisnis anda!
        </p>
        <div class="btn-cta">
            Contact Us
        </div>
    </div>
</section>