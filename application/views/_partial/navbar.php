<!-- NAVBAR -->

<nav class="navbar shadow">
    <a href="#">
        <img class="logo" src="<?= base_url("assets") ?>/asset/Logo.png" alt="logo">
    </a>

    <div class="nav-sm" id="nav-sm">
        <div class="nav-links ex-neu">
            <ul>
                <li><a href="#hero-sec" class="active in-neu">Beranda</a></li>
                <li><a href="#layanan-sec">Layanan</a></li>
                <li><a href="#porto-sec">Portofolio</a></li>
                <li><a href="#contact-sec">Kontak</a></li>
            </ul>
        </div>
        <div class="btn-login ex-neu">
            Sign In
        </div>
    </div>

    <div class="ham ex-neu" id="ham-menu">
        <span></span>
        <span></span>
        <span></span>
    </div>
</nav>

<!-- END NAVBAR -->