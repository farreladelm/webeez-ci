<footer class="footer">
    <div class="logo-container ex-neu">
        <a href="#">
            <img class="logo" src="<?= base_url("assets") ?>/asset/Logo.png" alt="logo">
        </a>
    </div>
    <div class="footer-content">
        <div class="details">
            <div class="detail">
                <i class="fa-solid fa-map-location-dot"></i>
                <span>Surabaya Barat</span>
            </div>
            <div class="detail">
                <i class="fa-solid fa-phone"></i>
                <span>Kerjasama : +628123456789</span>
            </div>
            <div class="detail">
                <i class="fa-solid fa-envelope"></i>
                <span>admin@webeez.id</span>
            </div>
        </div>
        <div class="navigasi">
            <h1>Navigasi</h1>
            <a href="#hero-sec">Beranda</a>
            <a href="#layanan-sec">Layanan</a>
            <a href="#porto-sec">Portofolio</a>
            <a href="#contact-sec">Kontak</a>
        </div>

    </div>
    <div class="social-media">
        <a href="#" class="ig ex-neu"><i class="fa-brands fa-instagram"></i>
        </a>
        <a href="#" class="fb ex-neu"><i class="fa-brands fa-square-facebook"></i></a>
        <a href="#" class="in ex-neu"><i class="fa-brands fa-linkedin"></i></a>
    </div>
    <div class="elipse"></div>
</footer>
<script src="<?= base_url("assets") ?>/js/script.js"></script>
</body>

</html>