<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?= base_url("assets") ?>/css/style.css">
    <link rel="stylesheet" href="<?= base_url("assets") ?>/css/all.min.css">
    <link rel="stylesheet" href="<?= base_url("assets") ?>/css/fontawesome.min.css">
    <script src="<?= base_url("assets") ?>/js/fontawesome.min.js"></script>
    <title>Webeez | Make Website with Ease</title>
</head>

<body>