const cards = document.getElementsByClassName("card");
const hamburger = document.getElementById("ham-menu");
const navbar = document.getElementById("nav-sm");

const navLinks = document.querySelectorAll(".nav-sm .nav-links ul li a");
const sections = document.querySelectorAll('.sec-listener');

const client1 = document.getElementById("client-1");
const client2 = document.getElementById("client-2");
const client3 = document.getElementById("client-3");

const images1 = ["assets/asset/client/1.png", "assets/asset/client/2.png", "assets/asset/client/3.png"];
const images2 = ["assets/asset/client/4.png", "assets/asset/client/5.png", "assets/asset/client/6.png"];
const images3 = ["assets/asset/client/7.png", "assets/asset/client/8.png", "assets/asset/client/9.png"];



for(const card of cards) {
    card.addEventListener("click", () => {
        card.children[0].classList.toggle("active");
        card.children[1].classList.toggle("active");
        console.log(card.children)
    })
}

hamburger.addEventListener("click", () => {
    hamburger.classList.toggle("active");
    navbar.classList.toggle("active");
})

navLinks.forEach((link) => {
    link.addEventListener("click", () => {
        hamburger.classList.remove("active");
        navbar.classList.remove("active");
        navLinks.forEach((i) => {
            i.classList.remove("active");
            i.classList.remove("in-neu");
        })
        link.classList.toggle("active");
        link.classList.toggle("in-neu");
    })
})

setInterval(() => {
    let random = Math.floor(Math.random() * 3)
    client1.src = images1[random];
    client2.src = images2[random];
    client3.src = images3[random];
}, 2000)

function changeLinkState() {
    let index = sections.length;
  
    while (--index && window.scrollY + 50 < sections[index].offsetTop) {}
  
    navLinks.forEach((link) => {
        link.classList.remove('active');
        link.classList.remove('in-neu');
    })
  
    // add the active class if within visible height of the element
    if (scrollY - sections[index].offsetHeight <
          sections[index].offsetTop) {
            navLinks[index].classList.add('active');
            navLinks[index].classList.add('in-neu');
    }
  }
  
  changeLinkState();
  window.addEventListener('scroll', changeLinkState);